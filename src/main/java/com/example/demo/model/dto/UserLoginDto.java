package com.example.demo.model.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class UserLoginDto implements Serializable {

    private String email;
    private String password;

    public UserLoginDto() {
    }

    @Email
    @Size(min = 8, max = 20, message = "password wrong: at least 8 symbols, maximum 20 symbols")
    @NotNull(message = "Email can not be empty")
    public String getEmail() {
        return email;
    }

    public UserLoginDto setEmail(String email) {
        this.email = email;
        return this;
    }

    @NotNull(message = "Password can not be empty")
    public String getPassword() {
        return password;
    }

    public UserLoginDto setPassword(String password) {
        this.password = password;
        return this;
    }
}
